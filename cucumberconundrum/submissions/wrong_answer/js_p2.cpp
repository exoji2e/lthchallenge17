#include <bits/stdc++.h>
using namespace std;

#define rep(i, a, b) for(int i = a; i < (b); ++i)
#define trav(a, x) for(auto& a : x)
#define all(x) x.begin(), x.end()
#define sz(x) (int)(x).size()
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

void P(int res) {
    cout << res << endl;
    exit(0);
}

bool C1(double x) { return x >= 1; }
bool C2(double x) { return x >= 2; }
bool C3(double x) { return x >= 1 + 2 * sqrt(3) / 3; }
bool C4(double x) { return x >= 1 + sqrt(2); }
bool C5(double x) { return x >= 1 + sqrt(2 * (1 + 1 / sqrt(5))); }
bool C6(double x) { return x >= 3; }
bool C7(double x) { return x >= 3; }

int main() {
	cin.sync_with_stdio(0); cin.tie(0);
	cin.exceptions(cin.failbit);

    double S, R;
    int N, Z;
    cin >> S >> R >> N >> Z;
    N = min(N, int((S * S) * (Z / 100.0) / (R * R)));

    S /= R;

    if (N >= 7 && C7(S)) P(7);
    if (N >= 6 && C6(S)) P(6);
    if (N >= 5 && C5(S)) P(5);
    if (N >= 4 && C4(S)) P(4);
    if (N >= 3 && C3(S)) P(3);
    //if (N >= 2 && C2(S)) P(2);
    if (N >= 1 && C1(S)) P(1);
    P(0);
}
