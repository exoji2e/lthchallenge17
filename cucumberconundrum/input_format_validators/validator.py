import math
import sys
import re

INT = "(0|[1-9][0-9]*)"
FLOAT = INT + "(\\.[0-9]{1,6})?"
RE = "^" + FLOAT + " " + FLOAT + " " + INT + " " + INT + "$"

line = sys.stdin.readline()
assert re.match(RE, line)
tokens = line.split()
s, r, n, z = float(tokens[0]), float(tokens[1]), int(tokens[2]), int(tokens[3])
assert 1 <= s <= 10
assert 0.5 <= r <= s
assert 1 <= n <= 7
assert 0 <= z <= 100

def solve(S, R, N, Z):
    def C1(x): return x >= 1
    def C2(x): return x >= 2
    def C3(x): return x >= 1 + 2 * math.sqrt(3) / 3
    def C4(x): return x >= 1 + math.sqrt(2)
    def C5(x): return x >= 1 + math.sqrt(2 * (1 + 1 / math.sqrt(5)))
    def C6(x): return x >= 3
    def C7(x): return x >= 3

    N = min(N, int((S * S) * (Z / 100.0) / (R * R)))
    S /= R

    if N >= 7 and C7(S): return 7
    if N >= 6 and C6(S): return 6
    if N >= 5 and C5(S): return 5
    if N >= 4 and C4(S): return 4
    if N >= 3 and C3(S): return 3
    if N >= 2 and C2(S): return 2
    if N >= 1 and C1(S): return 1
    return 0

assert solve(s - 1e-6, r, n, z) == solve(s + 1e-6, r, n, z)

line = sys.stdin.readline()
assert len(line) == 0
sys.exit(42)
