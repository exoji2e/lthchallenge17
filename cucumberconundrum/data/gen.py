import math
import random

def C1(): return 1
def C2(): return 2
def C3(): return 1 + 2 * math.sqrt(3) / 3
def C4(): return 1 + math.sqrt(2)
def C5(): return 1 + math.sqrt(2 * (1 + 1 / math.sqrt(5)))
def C6(): return 3
def C7(): return 3

CS = [C1, C2, C3, C4, C5, C6, C7]

def solve(S, R, N, Z):
    N = min(N, int((S * S) * (Z / 100.0) / (R * R)))
    S /= R
    if N >= 7 and S >= C7(): return 7
    if N >= 6 and S >= C6(): return 6
    if N >= 5 and S >= C5(): return 5
    if N >= 4 and S >= C4(): return 4
    if N >= 3 and S >= C3(): return 3
    if N >= 2 and S >= C2(): return 2
    if N >= 1 and S >= C1(): return 1
    return 0

cnt = 1
def wt(n, s, r, z):
    global cnt
    if solve(s - 1e-6, r, n, z) == solve(s + 1e-6, r, n, z):
        tc = "{:02d}.in".format(cnt)
        cnt += 1
        f = open(tc, "w")
        f.write("{:.6f} {:.6f} {:d} {:d}\n".format(s, r, n, z))
        f.close()

def ct1(n, s, r, z):
    s += 1e-5
    wt(n, s, r, z)

def ct2(n, s, r, z):
    s -= 1e-5
    if s >= r: wt(n, s, r, z)

def ct3(n, s, r, z):
    if n > 1:
        n -= 1
        wt(n, s, r, z)

def ct4(n, s, r, z):
    if n < 7:
        n += 1
        wt(n, s, r, z)

def ct5(n, s, r, z):
    sol = solve(n, s, r, z)
    if not sol: proc = 0
    else: proc = int(s*s / (sol * r * r))
    wt(n, s, r, proc)

def ct6(n, s, r, z):
    sol = solve(n, s, r, z)
    if not sol: proc = 0
    else: proc = int(s*s / (sol * r * r))
    wt(n, s, r, proc + 1)

for n in range(1, 8):
    s = CS[n - 1]()
    r = 1
    z = 100

    ct1(n, s, r, z)
    ct2(n, s, r, z)
    ct3(n, s, r, z)
    ct4(n, s, r, z)
    ct5(n, s, r, z)
    ct6(n, s, r, z)

for k in range(30):
    s = random.randint(1, 9) + random.randint(0, 999999) / 1000000
    r = random.randint(0, 10) + random.randint(0, 999999) / 1000000
    while not (0.5 <= r <= s):
        r = random.randint(0, 10) + random.randint(0, 999999) / 1000000
    n = random.randint(1, 7)
    z = random.randint(0, 100)
    wt(n, s, r, z)
