import java.util.*;
import java.io.*;
class MinCostMaxFlow {
    boolean found[];
    int N, dad[];
    long cap[][], flow[][], cost[][], dist[], pi[];
    
    static final long INF = Long.MAX_VALUE / 2 - 1;
    
    boolean search(int source, int sink) {
	Arrays.fill(found, false);
	Arrays.fill(dist, INF);
	dist[source] = 0;

	while (source != N) {
	    int best = N;
	    found[source] = true;
	    for (int k = 0; k < N; k++) {
		if (found[k]) continue;
		if (flow[k][source] != 0) {
		    long val = dist[source] + pi[source] - pi[k] - cost[k][source];
		    if (dist[k] > val) {
			dist[k] = val;
			dad[k] = source;
		    }
		}
		if (flow[source][k] < cap[source][k]) {
		    long val = dist[source] + pi[source] - pi[k] + cost[source][k];
		    if (dist[k] > val) {
			dist[k] = val;
			dad[k] = source;
		    }
		}
		
		if (dist[k] < dist[best]) best = k;
	    }
	    source = best;
	}
	for (int k = 0; k < N; k++)
	    pi[k] = Math.min(pi[k] + dist[k], INF);
	return found[sink];
    }
    
    
    long[] getMaxFlow(long cap[][], long cost[][], int source, int sink) {
	this.cap = cap;
	this.cost = cost;
	
	N = cap.length;
        found = new boolean[N];
        flow = new long[N][N];
        dist = new long[N+1];
        dad = new int[N];
        pi = new long[N];
	
	long totflow = 0, totcost = 0;
	while (search(source, sink)) {
	    long amt = INF;
	    for (int x = sink; x != source; x = dad[x])
		amt = Math.min(amt, flow[x][dad[x]] != 0 ? flow[x][dad[x]] :
                       cap[dad[x]][x] - flow[dad[x]][x]);
	    for (int x = sink; x != source; x = dad[x]) {
		if (flow[x][dad[x]] != 0) {
		    flow[x][dad[x]] -= amt;
		    totcost -= amt * cost[x][dad[x]];
		} else {
		    flow[dad[x]][x] += amt;
		    totcost += amt * cost[dad[x]][x];
		}
	    }
	    totflow += amt;
	}
	
	return new long[]{ totflow, totcost };
    }
}
public class mm {
    void solve(BufferedReader in) throws Exception {
        int n = toInt(in.readLine());
        int[][] items = new int[n][2];
        for(int i = 0; i<n; i++) items[i] = toInts(in.readLine());
        int s = toInt(in.readLine());
        int sz = 2 + n + s;
        long[][] cost = new long[sz][sz];
        long[][] cap = new long[sz][sz];
        for(int i = 0; i<s; i++) {
            int[] a = toInts(in.readLine());
            for(int j = 2; j<a.length; j++) cap[a[j]][i+n+1] = a[0];
            cap[i+n+1][sz-1] = a[0];
        }
        for(int i = 0; i<n; i++) {
            cost[0][i+1] = -items[i][0];
            cap[0][i+1] = items[i][1];
        }
        MinCostMaxFlow flow = new MinCostMaxFlow();
        long r[] = flow.getMaxFlow(cap, cost, 0, sz-1);
        System.out.println(-r[1]/100.0);
    }
    int toInt(String s) {return Integer.parseInt(s);}
    int[] toInts(String s) {
        String[] a = s.split(" ");
        int[] o = new int[a.length];
        for(int i = 0; i<a.length; i++) o[i] = toInt(a[i]);
        return o;
    }
    void e(Object o) {
        System.err.println(o);
    }
    public static void main(String[] args) throws Exception{
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        (new mm()).solve(in);
    }
}
