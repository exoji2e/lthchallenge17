#include <bits/stdc++.h>
using namespace std;

#define rep(i, a, b) for(int i = a; i < (b); ++i)
#define trav(a, x) for(auto& a : x)
#define all(x) x.begin(), x.end()
#define sz(x) (int)(x).size()
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

template<class T> T edmondsKarp(vector<unordered_map<int, T>>& graph, int source, int sink, int cats) {
	assert(source != sink);
	T flow = 0;
	vi par(sz(graph)), q = par;

    int cat = 1;
	for (;;) {
		fill(all(par), -1);
		par[source] = 0;
		int ptr = 1;
		q[0] = source;

		rep(i,0,ptr) {
			int x = q[i];
			trav(e, graph[x]) {
                if (1 <= e.first && e.first <= cats && e.first > cat) continue;
				if (par[e.first] == -1 && e.second > 0) {
					par[e.first] = x;
					q[ptr++] = e.first;
					if (e.first == sink) goto out;
				}
			}
		}
        if (cat != cats) {
            ++cat;
            continue;
        }
		return flow;
out:
		T inc = numeric_limits<T>::max();
		for (int y = sink; y != source; y = par[y])
			inc = min(inc, graph[par[y]][y]);

		flow += inc;
		for (int y = sink; y != source; y = par[y]) {
			int p = par[y];
			if ((graph[p][y] -= inc) <= 0) graph[p].erase(y);
			graph[y][p] += inc;
		}
	}
}

int main() {
	cin.sync_with_stdio(0); cin.tie(0);
	cin.exceptions(cin.failbit);

    int N;
    cin >> N;

    vector<tuple<int, int, int>> TP(N);
    rep(i,0,N) {
        int a, b;
        cin >> a >> b;
        TP.emplace_back(-a, b, i);
    }
    sort(all(TP));
    vi remap(N);
    rep(i,0,N) remap[get<2>(TP[i])] = i;

    int S;
    cin >> S;

#define SOURCE 0
#define CAT(x) (SOURCE + 1) + x
#define STORE(y) CAT(N) + y
#define SINK STORE(S)

    vector<unordered_map<int, ll>> G(SINK + 1);
    rep(i,0,N) {
        G[SOURCE][CAT(i)] = get<1>(TP[i]) - 1;
    }

    rep(i,0,S) {
        int lim, cnt;
        cin >> lim >> cnt;
        G[STORE(i)][SINK] = lim;
        rep(j,0,cnt) {
            int cat;
            cin >> cat;
            G[CAT(remap[cat - 1])][STORE(i)] = lim;
        }
    }

    edmondsKarp(G, SOURCE, SINK, N);

    double res = 0;
    rep(i,0,N) {
        res += (-get<0>(TP[i])) / 100.0 * (get<1>(TP[i]) - G[SOURCE][CAT(remap[get<2>(TP[i])])]);
    }
    cout << setprecision(9) << fixed << res << endl;

}
