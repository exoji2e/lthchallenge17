import random

N = 300
S = 300
lims = [random.randint(1, 10**9) for _ in range(N)]
rates = [random.randint(0, 99) for _ in range(N)]

print(N)
for i in range(N):
    print("{} {}".format(rates[i], lims[i]))

print(S)
for i in range(S):
    s = random.sample(list(range(1, N + 1)), random.randint(1, N))
    print("{} {} {}".format(random.randint(1, 10**9), len(s), ' '.join(str(x) for x in s)))
