import random

N = 700
S = 700
lims = [10**9 for _ in range(N)]
rates = [99 for _ in range(N)]

print(N)
for i in range(N):
    print("{} {}".format(rates[i], lims[i]))

print(S)
for i in range(S):
    s = list(range(1, N + 1))
    print("{} {} {}".format(10**9, len(s), ' '.join(str(x) for x in s)))
