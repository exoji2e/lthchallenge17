import java.util.*;
import java.io.*;
public class mm {

    class Tree {
        ArrayList<Edge> childs = new ArrayList<>();
        int[][][] dp = new int[301][100][2];
        public int reach(Tree from, int timeLeft, int stay, int at) {
            if(timeLeft < 0) return -1000;
            if (at == childs.size()) return 1;
            if (childs.get(at).to == from) return reach(from, timeLeft, stay, at+1);
            if(dp[timeLeft][at][stay] != -1) return dp[timeLeft][at][stay];
            int max = this.reach(from, timeLeft, stay, at + 1);
            Tree nxt = childs.get(at).to;
            int cost = childs.get(at).cost;
            for(int t = 0; t<=timeLeft; t++) {
                if(stay == 1)
                    max = Math.max(max, nxt.reach(this, t, 1, 0) + this.reach(from, timeLeft - t - cost, 0, at + 1));
                max = Math.max(max, nxt.reach(this, t, 0, 0) + this.reach(from, timeLeft - t - 2*cost, stay, at + 1));
            }
            dp[timeLeft][at][stay] = max;
            return max;
        }
        public Tree() {
            for(int i = 0; i<301; i++)
                for(int j = 0; j<100; j++)
                    for(int k = 0; k<2; k++)
                        dp[i][j][k] = -1;
        }
    }
    class Edge {
        Tree to;
        int cost;
    }
    void solve(BufferedReader in) throws Exception {
        int[] xx = toInts(in.readLine());
        int m = xx[0], n = xx[1];
        Tree[] trees = new Tree[m];
        for(int i = 0; i<m; i++) trees[i] = new Tree();
        for(int i = 1; i<m; i++) {
            xx = toInts(in.readLine());
            int u = xx[0], v = xx[1], t = xx[2];
            Edge e1 = new Edge();
            Edge e2 = new Edge();
            e1.to = trees[v];
            e2.to = trees[u];
            e1.cost = t;
            e2.cost = t;
            trees[u].childs.add(e1);
            trees[v].childs.add(e2);
        }
        System.out.println(trees[0].reach(null, n, 1, 0) -1);

    }
    int toInt(String s) {return Integer.parseInt(s);}
    int[] toInts(String s) {
        String[] a = s.split(" ");
        int[] o = new int[a.length];
        for(int i = 0; i<a.length; i++) o[i] = toInt(a[i]);
        return o;
    }
    void e(Object o) {
        System.err.println(o);
    }
    public static void main(String[] args) throws Exception{
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        (new mm()).solve(in);
    }
}
