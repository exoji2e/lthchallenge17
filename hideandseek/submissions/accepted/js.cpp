#include <vector>
#include <iostream>

using namespace std;

const int MAX_N = 100;
const int MAX_TIME = 100;
const int INF = MAX_N;

typedef pair<int, int> pii;

vector<vector<vector<pii>>> dp; 

int solve(int at, int timeLeft, int atChild, bool canStay, vector<vector<pair<int, int>>>& children){
	if(timeLeft < 0) return -INF;
	if(atChild == (int)children[at].size()) return 1;
	int& ans = canStay ? dp[at][timeLeft][atChild].first : dp[at][timeLeft][atChild].second;
	int theChild = children[at][atChild].first;
	int childTime = children[at][atChild].second;
	if(ans == -1){
		ans = 0;

		for(int timeAlloc = 0; timeAlloc <= timeLeft; timeAlloc++){
			if(canStay) ans = max(ans, solve(theChild, timeAlloc, 0, true, children) + solve(at, timeLeft - childTime - timeAlloc, atChild + 1, false, children));
			ans = max(ans, solve(theChild, timeAlloc, 0, false, children) + solve(at, timeLeft - 2 * childTime - timeAlloc, atChild + 1, canStay, children));
		}   
		ans = max(ans, solve(at, timeLeft, atChild + 1, canStay, children));
	}   
	return ans;
}

int main(){
	int N, T;
	cin >> N >> T;
	dp.assign(N, vector<vector<pii>>(T + 1));
	vector<vector<pair<int, int>>> children(N);
	for(int i = 0; i < N - 1; ++i){
		int a, b, time;
		cin >> a >> b >> time;
		children[a].push_back(make_pair(b, time));
	}   
	for(int i = 0; i < N; ++i){
		for(int j = 0; j <= T; ++j){
			dp[i][j].assign(children[i].size(), pii(-1, -1));
		}   
	}   
	cout << solve(0, T, 0, true, children) - 1 << endl;
}

