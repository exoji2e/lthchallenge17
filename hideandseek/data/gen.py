import math
import random

class Instance(object):

    def __init__(self, time):
        self.vertices = 1
        self.time = time
        self.edges = []

    def add_child(self, parent, length):
        assert self.vertices > parent
        self.edges.append((parent, self.vertices, length))
        self.vertices += 1

cnt = 1
def wt(instance):
    global cnt
    tc = "{:02d}.in".format(cnt)
    cnt += 1
    f = open(tc, "w")
    f.write("{} {}\n".format(instance.vertices, instance.time))
    assert len(instance.edges) == instance.vertices - 1
    assert 1 <= instance.vertices <= 100
    assert 0 <= instance.time <= 300
    for a, b, length in instance.edges:
        f.write("{} {} {}\n".format(a, b, length))
    f.close()

def rcase():
    instance = Instance(300)
    for i in range(99):
        instance.add_child(random.randint(0, i), random.randint(1, 30))
    wt(instance)

def rcase2():
    instance = Instance(300)
    for i in range(99):
        instance.add_child(random.randint(0, i), random.randint(1, 2))
    wt(instance)

def line():
    instance = Instance(300)
    for i in range(99):
        instance.add_child(i, random.randint(1, 10))
    wt(instance)

def line2():
    instance = Instance(300)
    ts = [0, 0]
    for i in range(99):
        v = random.randint(0, 1)
        instance.add_child(ts[v], random.randint(1, 10))
        ts[v] = i + 1
    wt(instance)

def empty():
    instance = Instance(1)
    instance.add_child(0, 1)
    wt(instance)

def lineall():
    instance = Instance(300)
    for i in range(99):
        instance.add_child(i, 3)
    wt(instance)

def binall():
    instance = Instance(300)
    for i in range(99):
        instance.add_child(i // 2, 1)
    wt(instance)

for i in range(10): rcase()
for i in range(5): rcase2()
line()
line()
line2()
line2()
empty()
lineall()
binall()
