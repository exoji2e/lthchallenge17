#!/usr/bin/env python3
from collections import deque
hh1, mm1 = input().split(":")
hh2, mm2 = input().split(":")
def valid(t):
    h = t[0]*10 + t[1]
    m = t[2]*10 + t[3]
    return min(t) >= 0 and max(t) < 10 and h < 24 and m < 60

def totime(t):
    s = ''.join(map(str, t))
    return s[:2] + ':' + s[2:]

d = {}
q = deque()
s = tuple(map(int, [hh1[0], hh1[1], mm1[0], mm1[1]]))
t = tuple(map(int, [hh2[0], hh2[1], mm2[0], mm2[1]]))
q.append(s)
d[s] = (0, 0)
pp = False
while q:
    nxt = q.popleft()
    if nxt == t:
        out = []
        while nxt:
            out.append(nxt)
            nxt = d[nxt][1]
        
        print(len(out))
        print('\n'.join(map(totime, reversed(out))))
        pp = True
        break
    for i in range(4):
        l = list(nxt)
        l[i] = (l[i] + 1)%10
        c = tuple(l)
        if valid(c) and not c in d:
            d[c] = (d[nxt][0] + 1, nxt)
            q.append(c)
        l[i] = (l[i] - 2)%10
        c = tuple(l)
        if valid(c) and not c in d:
            d[c] = (d[nxt][0] + 1, nxt)
            q.append(c)
     
