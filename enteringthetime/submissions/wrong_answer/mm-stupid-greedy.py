#!/usr/bin/env python3
def t2str(t):
    h1, h2, m1, m2 = t
    h = str(h1) + str(h2)
    m = str(m1) + str(m2)
    return h + ':' + m

def str2t(s):
    h, m = s.split(':')
    return list(map(int, [h[0], h[1], m[0], m[1]]))

t0 = str2t(input())
tn = str2t(input())
out = [t2str(t0)]
for i in range(4):
    if t0[i] != tn[i]:
        if (t0[i] - tn[i])%10 <= (tn[i] - t0[i])%10:
            while t0[i] != tn[i]:
                t0[i] = (t0[i] - 1)%10
                out.append(t2str(t0))
        else:
            while t0[i] != tn[i]:
                t0[i] = (t0[i] + 1)%10
                out.append(t2str(t0))
print(len(out))
print('\n'.join(out))

