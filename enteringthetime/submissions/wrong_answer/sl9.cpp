#include <bits/stdc++.h>
using namespace std;

#define rep(i, from, to) for (int i = from; i < (to); ++i)
#define trav(a, x) for (auto& a : x)
#define all(x) x.begin(), x.end()
#define sz(x) (int)(x).size()
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

int main() {
	cin.sync_with_stdio(false);
	string from, to;
	cin >> from >> to;
	vector<string> res;
	assert(sz(from) == 5);
	assert(sz(to) == 5);
	assert(from[2] == ':');
	assert(to[2] == ':');
	while (from != to) {
		res.push_back(from);
		if (from[0] != to[0] && from[0] == '2') {
			--from[0];
			continue;
		}

		int ind = 4;
		while (from[ind] == to[ind]) --ind;

		int distup = to[ind] - from[ind];
		if (distup < 0) distup += 10;
		int distdown = from[ind] - to[ind];
		if (distdown < 0) distdown += 10;

		if (distup < distdown) {
			if (from[ind]++ == '9') from[ind] = '0';
		} else {
			if (from[ind]-- == '0') from[ind] = '9';
		}
	}
	res.push_back(to);

	cout << sz(res) << endl;
	trav(x, res) cout << x << endl;
}
