#!/usr/bin/python3

import sys
import random
from random import randint

def rndtime():
    h = randint(0, 23)
    m = randint(0, 59)
    h = str(h) if h >= 10 else '0' + str(h)
    m = str(m) if m >= 10 else '0' + str(m)
    return h + ':' + m

def main():
    random.seed(int(sys.argv[-1]))
    print(rndtime())
    print(rndtime())

if __name__ == "__main__":
    main()
