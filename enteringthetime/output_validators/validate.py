#!/usr/bin/python3
import sys, re

CLOCK_REGEX = '^[0-9][0-9]:[0-9][0-9]$'

def totime(s):
    h, m = s.split(':')
    return list(map(int, [h[0], h[1], m[0], m[1]]))
def ok(line):
    if not re.match(CLOCK_REGEX, line): return False
    h, m = map(int, line.split(':'))
    return 0 <= h <= 24 and 0 <= m <= 59

def validstep(s, t):
    diffs = 0
    for i in range(4):
        if s[i] != t[i]:
            diffs += 1
            if abs(s[i]-t[i]) > 1 and not (min(s[i], t[i]) == 0 and max(s[i], t[i]) == 9):
                return False
    return diffs == 1

inp = open(sys.argv[1], 'r')
judge = open(sys.argv[2], 'r')

t0 = inp.readline().strip()
tn = inp.readline().strip()

n0 = int(judge.readline().strip())
try:
    n = int(sys.stdin.readline().strip())
except:
    print('first line is not an integer')
    sys.exit(43)

if n0 < n:
    print('there exists faster strastegy')
    sys.exit(43)
try:
    t = sys.stdin.readline().strip()
except:
    print('excpected more input')
    sys.exit(43)

if not ok(t) or t != t0:
    print('first timestamp is not equal to t0', t, t0, ok(t))
    sys.exit(43)
for i in range(n-1):
    try:
        tnew = sys.stdin.readline().strip()
    except:
        print('excpected more input')
        sys.exit(43)
    if not ok(tnew):
        print('malformed timestamp')
        sys.exit(43)
    if not validstep(totime(t), totime(tnew)):
        print('invalid step')
        sys.exit(43)
    t = tnew
if t != tn:
    print('doesn\'t end with tn as last timestamp')
    sys.exit(43)
try:
    x = sys.stdin.readline()
    if x != '':
        print('input not finished')
        sys.exit(43)
except:
    print('eof as expected')   
print('ok')
sys.exit(42)
