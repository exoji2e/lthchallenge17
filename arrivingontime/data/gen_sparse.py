#!/usr/bin/python3
import sys
import random

default = {
    "n": 100000
}

def pair(n):
    a = random.randint(0, n-1)
    b = random.randint(0, n-2)
    b += 1 if b >=a else 0
    return (a, b)

def cmdlinearg(name):
    for arg in sys.argv:
        if arg.startswith(name + "="):
            return arg.split("=")[1]
    return default[name]

def main():
    random.seed(int(sys.argv[-1]))
    n  = int(cmdlinearg('n'))
    assert 3 < n <= default['n']
    m = 2*n
    s, e = pair(n)
    s, e = (0, n-1)
    t = random.randint(100000000, 1000000000)
    print("{} {} {}".format(n, m, t))
    roads = set()
    for i in range(0, m):
        tup = pair(n)
        while tup in roads:
            tup = pair(n)
        roads.add(tup)
        u, v = tup
        t0 = random.randint(0, 10000)
        P = random.randint(1000, 10000)
        d = random.randint(1000, 10000)
        print("{} {} {} {} {}".format(u, v, t0, P, d))

if __name__ == "__main__":
    main()
