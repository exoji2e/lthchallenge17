#!/bin/bash
for r in 2478 6657 4107 5305 5598 7972 3032 8927 9035 5601
do
    echo $r
    python3 gen_fully_connected.py $r > "secret/$r.fully.in"
    python3 gen_sparse.py $r > "secret/$r.sparse.in"
    python3 gen_sparse_large.py $r > "secret/$r.sparse-large.in"
    python3 gen_line.py $r > "secret/$r.line.in"
done
