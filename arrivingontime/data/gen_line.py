#!/usr/bin/python3
import sys
import random

default = {
    "n": 100000
}

def cmdlinearg(name):
    for arg in sys.argv:
        if arg.startswith(name + "="):
            return arg.split("=")[1]
    return default[name]

def main():
    random.seed(int(sys.argv[-1]))
    n = int(cmdlinearg('n'))
    assert n > 1
    m = n - 1
    t = random.randint(100000000, 1000000000)
    print("{} {} {}".format(n, m, t))
    for i in range(0, m):
        u = i
        v = i + 1
        t0 = random.randint(0, 10000)
        P = random.randint(1000, 10000)
        d = random.randint(1000, 10000)
        print("{} {} {} {} {}".format(u, v, t0, P, d))

if __name__ == "__main__":
    main()
