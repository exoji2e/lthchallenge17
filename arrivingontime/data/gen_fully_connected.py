#!/usr/bin/python3

import sys
import random

default = {
    "n": 440
}

def cmdlinearg(name):
    for arg in sys.argv:
        if arg.startswith(name + "="):
            return arg.split("=")[1]
    return default[name]

def main():
    random.seed(int(sys.argv[-1]))
    n = int(cmdlinearg('n'))
    assert n > 1
    m = n*(n-1)
    s = random.randint(0, n-1)
    e = random.randint(0, n-2)
    s = 0
    e = n-1
    t = random.randint(100000000, 1000000000)
    print("{} {} {}".format(n, m, t))
    for i in range(0, n*n):
        u = i//n
        v = i%n
        t0 = random.randint(0, 10000)
        P = random.randint(1000, 1000000)
        d = random.randint(1000, 1000000)
        if u == s:
            d = random.randint(1000000, t - t0)
            if v == e:
                t0 = t - d
        if u == v: continue
        print("{} {} {} {} {}".format(u, v, t0, P, d))

if __name__ == "__main__":
    main()
