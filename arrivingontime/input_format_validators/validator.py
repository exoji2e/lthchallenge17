#!/usr/bin/python

import re
import sys

INT_REGEX = '(([1-9][0-9]*)|0)'
THREE_INTS_REGEX = '^' + ' '.join([INT_REGEX] * 3) + '$'
FIVE_INTS_REGEX = '^' + ' '.join([INT_REGEX] * 5) + '$'

line = sys.stdin.readline()
assert re.match(THREE_INTS_REGEX, line)
n, m, s  = map(int, line.split())
assert 2 <= n <= 100000
assert 1 <= m <= 200000
assert 1 <= s <= 10 ** 9

roads = set()
for i in range(m):
    line = sys.stdin.readline()
    assert re.match(FIVE_INTS_REGEX, line)
    u, v, t0, p, d = map(int, line.split())
    assert 0 <= u < n
    assert 0 <= v < n
    assert u != v
    assert 0 <= t0 <= 10 ** 9
    assert 1 <= p <= 10 ** 9
    assert 1 <= d <= 10 ** 9
    assert (u, v) not in roads
    roads.add((u, v))

assert len(sys.stdin.readline()) == 0

sys.exit(42)
