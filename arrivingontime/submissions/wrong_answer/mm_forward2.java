import java.util.*;
import java.io.*;
public class mm_forward2 {
    class Node implements Comparable<Node> {
        int id;
        long w = Long.MAX_VALUE;
        LinkedList<Edge> edgs = new LinkedList<>();
        public Node(int id) {this.id = id;}
        public int compareTo(Node n) {
            if(w != n.w) return Long.compare(w, n.w);
            return id - n.id;
        }
        public long djikstra(Node end, long t) {
            TreeSet<Node> set = new TreeSet<>();
            w = t;
            set.add(this);
            while(!set.isEmpty()) {
                Node nxt = set.pollFirst();
                if(nxt == end) break;
                for(Edge e: nxt.edgs) {
                    long take = e.t + ((Math.max(0,nxt.w - e.t) + e.P - 1)/e.P)*e.P;
                    long arrive = take + e.d;
                    if(arrive < e.to.w) {
                        set.remove(e.to);
                        e.to.w = arrive;
                        set.add(e.to);
                    }
                }
            }
            return end.w;
        }
    }
    class Edge {
        Node to;
        int t, P, d;
        public Edge(Node to, int t, int P, int d) {
            this.to = to; this.t = t; this.P=P;
            this.d=d;
        }
    }
    void solve(BufferedReader in) throws Exception {
        int[] xx = toInts(in.readLine());
        int N = xx[0], M = xx[1], s = xx[2];
        Node[] nodes = new Node[N];
        for(int i = 0; i<N; i++) nodes[i] = new Node(i);
        for(int i = 0; i<M; i++) {
            xx = toInts(in.readLine());
            int u = xx[0], v = xx[1], t0 = xx[2], P = xx[3], d = xx[4];
            nodes[u].edgs.add(new Edge(nodes[v], t0, P, d));
        }
        long dist = s - nodes[0].djikstra(nodes[N-1], 0);
        if(dist < 0) System.out.println("impossible");
        else System.out.println(dist);
    }
    int toInt(String s) {return Integer.parseInt(s);}
    int[] toInts(String s) {
        String[] a = s.split(" ");
        int[] o = new int[a.length];
        for(int i = 0; i<a.length; i++) o[i] = toInt(a[i]);
        return o;
    }
    void e(Object o) {
        System.err.println(o);
    }
    public static void main(String[] args) throws Exception{
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        (new mm_forward2()).solve(in);
    }
}
