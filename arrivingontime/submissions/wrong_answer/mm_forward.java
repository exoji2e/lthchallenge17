import java.util.*;
import java.io.*;
public class mm_forward {
    class Node implements Comparable<Node> {
        int id;
        long w = -1;
        LinkedList<Edge> edgs = new LinkedList<>();
        public Node(int id) {this.id = id;}
        public int compareTo(Node n) {
            if(w != n.w) return Long.compare(w, n.w);
            return id - n.id;
        }
        public long djikstra(Node end, long t) {
            TreeSet<Node> set = new TreeSet<>();
            w = t;
            set.add(this);
            while(!set.isEmpty()) {
                Node nxt = set.pollLast();
                if(nxt == end) break;
                for(Edge e: nxt.edgs) {
                    long fst = e.t + e.d;
                    if(fst > nxt.w) continue;
                    long last = ((nxt.w - fst)/e.P)*e.P + fst;
                    if(last - e.d > e.to.w) {
                        set.remove(e.to);
                        e.to.w = last - e.d;
                        set.add(e.to);
                    }
                }
            }
            return end.w;
        }
    }
    class Edge {
        Node to;
        int t, P, d;
        public Edge(Node to, int t, int P, int d) {
            this.to = to; this.t = t; this.P=P;
            this.d=d;
        }
    }
    void solve(BufferedReader in) throws Exception {
        int[] xx = toInts(in.readLine());
        int N = xx[0], M = xx[1], s = xx[2];
        Node[] nodes = new Node[N];
        for(int i = 0; i<N; i++) nodes[i] = new Node(i);
        for(int i = 0; i<M; i++) {
            xx = toInts(in.readLine());
            int u = xx[0], v = xx[1], t0 = xx[2], P = xx[3], d = xx[4];
            nodes[v].edgs.add(new Edge(nodes[u], t0, P, d));
        }
        long dist = nodes[0].djikstra(nodes[N-1], s);
        if(dist == -1) System.out.println("impossible");
        else System.out.println(dist);
    }
    int toInt(String s) {return Integer.parseInt(s);}
    int[] toInts(String s) {
        String[] a = s.split(" ");
        int[] o = new int[a.length];
        for(int i = 0; i<a.length; i++) o[i] = toInt(a[i]);
        return o;
    }
    void e(Object o) {
        System.err.println(o);
    }
    public static void main(String[] args) throws Exception{
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        (new mm_forward()).solve(in);
    }
}
