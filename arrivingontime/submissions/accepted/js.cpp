#include <bits/stdc++.h>
using namespace std;

#define rep(i, a, b) for(int i = a; i < (b); ++i)
#define trav(a, x) for(auto& a : x)
#define all(x) x.begin(), x.end()
#define sz(x) (int)(x).size()
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;


struct T {
    int u, v, t0, p, d;

    T(int u, int v, int t0, int p, int d) :
        u(u), v(v), t0(t0), p(p), d(d) {}

    int earliest(int target) {
        // k >= 0
        // t0 + d + kp <= target
        // kp <= target - t0 - d
        int mkp = target - t0 - d;
        if (mkp < 0) return -1;
        int k = mkp / p;
        return t0 + k * p;
    }
};

int main() {
	cin.sync_with_stdio(0); cin.tie(0);
	cin.exceptions(cin.failbit);

    int n, m, s;
    cin >> n >> m >> s;

    vector<vector<T>> trams(n);
    rep(i,0,m) {
        int u, v, t0, p, d;
        cin >> u >> v >> t0 >> p >> d;
        trams[v].emplace_back(u, v, t0, p, d);
    }

    vector<int> besttime(n, -1);
    besttime[n - 1] = s;
    priority_queue<pii> q;
    q.emplace(s, n - 1);

    while (!q.empty()) {
        int cur, t;
        tie(t, cur) = q.top();
        q.pop();

        if (t != besttime[cur]) continue;
        trav(it, trams[cur]) {
            int nt = it.earliest(t);
            if (nt >= besttime[it.u]) {
                besttime[it.u] = nt;
                q.emplace(nt, it.u);
            }
        }

    }

    if (besttime[0] >= 0) {
        cout << besttime[0] << endl;
    } else cout << "impossible" << endl;

}
