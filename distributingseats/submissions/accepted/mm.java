import java.util.*;
import java.io.*;
public class mm {
    class Seg implements Comparable<Seg> {
        int s, e;
        public int compareTo(Seg s) {
            return e - s.e;
        }
    }
    void solve(BufferedReader in) throws Exception {
        int[]  xx = toInts(in.readLine());
        int n = xx[0], r = xx[1], c = xx[2];
        Seg[] segs = new Seg[n];
        for(int i = 0; i<n; i++) {
            xx = toInts(in.readLine());
            Seg s = new Seg();
            s.s = Math.max(1, xx[0] - xx[2]);
            s.e = Math.min(r, xx[0] + xx[2]);
            segs[i] = s;
        }
        Arrays.sort(segs);
        TreeMap<Integer, Integer> map = new TreeMap<>();
        for(int i = 1; i<=r; i++) {
            map.put(i, c);
        }
        int ok = 0;
        for(Seg s: segs) {
            Integer k = map.higherKey(s.s -1);
            if(k == null) continue;
            if(k > s.e) continue;
            ok++;
            if(map.get(k) == 1) {
                map.remove(k);
            } else {
                map.put(k, map.get(k) -1);
            }
        }
        System.out.println(ok);

    }
    int toInt(String s) {return Integer.parseInt(s);}
    int[] toInts(String s) {
        String[] a = s.split(" ");
        int[] o = new int[a.length];
        for(int i = 0; i<a.length; i++) o[i] = toInt(a[i]);
        return o;
    }
    void e(Object o) {
        System.err.println(o);
    }
    public static void main(String[] args) throws Exception{
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        (new mm()).solve(in);
    }
}
