#!/usr/local/bin/pypy
n, r, c = map(int, raw_input().split())
pas = [tuple(map(int, raw_input().split())) for _ in range(n)]
segs = sorted(list(map(lambda t: (max(t[0] - 1 - t[2], 0), min(t[0] + t[2], r)), pas)), key=lambda x: (x[1], x[0]))
left = [c]*r
import math
sq = int(math.sqrt(r))
nbr = (r + sq - 1)//sq
sqrts = [c*sq]*nbr
sqrts[-1] = r*c - (c*sq*(nbr-1))
ok = 0
for s in segs:
    f1 = s[0]//sq
    f2 = (s[1]-1)//sq
    for f in range(f1, f2+1):
        if sqrts[f]:
            found = False
            for i in range(max(s[0], f*sq), min(s[1], f*sq + sq)):
                if left[i]:
                    ok += 1
                    left[i] -= 1
                    sqrts[f] -= 1
                    found = True
                    break
            if found:
                break
print(ok)
