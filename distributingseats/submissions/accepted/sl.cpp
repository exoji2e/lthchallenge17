#include <bits/stdc++.h>
using namespace std;

#define rep(i, from, to) for (int i = from; i < (to); ++i)
#define trav(a, x) for (auto& a : x)
#define all(x) x.begin(), x.end()
#define sz(x) (int)(x).size()
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

int main() {
	cin.sync_with_stdio(false);
	int N, R, C;
	cin >> N >> R >> C;
	vector<vi> ev(R);
	rep(i,0,N) {
		int a, b, s;
		cin >> a >> b >> s;
		a--;
		int mi = max(0, a - s);
		int ma = min(R-1, a + s);
		ev[mi].push_back(ma);
	}

	multiset<int> s;
	int res = 0;
	rep(r,0,R) {
		while (!s.empty() && *s.begin() < r)
			s.erase(s.begin());
		trav(e, ev[r]) s.insert(e);
		int co = 0;
		while (co < C && !s.empty()) {
			s.erase(s.begin());
			res++;
			co++;
		}
	}
	cout << res << endl;
}
