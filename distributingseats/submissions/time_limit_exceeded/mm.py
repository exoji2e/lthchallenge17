#!/usr/local/bin/pypy
n, r, c = map(int, raw_input().split())
pas = [tuple(map(int, raw_input().split())) for _ in range(n)]
segs = sorted(list(map(lambda t: (max(t[0] - 1 - t[2], 0), min(t[0] - 1 + t[2], r)), pas)),key=lambda x: (x[1], x[0]))
left = [c]*r
ok = 0
for j, s in enumerate(segs):
    for i in range(s[0], s[1] + 1):
        if left[i] > 0:
            left[i] -= 1
            ok += 1
            break
print(ok)
