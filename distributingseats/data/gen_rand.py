#!/usr/bin/python3

import sys
import random
from random import randint

default = {
    "n": 100000,
    "c": 1,
    "r": 100000
}

def cmdlinearg(name):
    for arg in sys.argv:
        if arg.startswith(name + "="):
            return arg.split("=")[1]
    return default[name]

def main():
    random.seed(int(sys.argv[-1]))
    n, r, c = (int(cmdlinearg(name)) for name in ['n', 'r', 'c'])
    assert n > 1
    print("{} {} {}".format(n, r, c))
    trips = []
    import math
    log = int(math.log2(r))
    for i in range(n):
        trips.append((randint(1, r), randint(1, c), randint(0, min(r, log))))
    for r, c, d in trips:
        print("{} {} {}".format(r, c, d))

if __name__ == "__main__":
    main()
