#!/usr/bin/python3

import sys
import random

default = {
    "n": 100000,
    "c": 1,
    "r": 100000,
    "padl" : 1000,
    "padr" : 1000,
}

def cmdlinearg(name):
    for arg in sys.argv:
        if arg.startswith(name + "="):
            return arg.split("=")[1]
    return default[name]

def main():
    random.seed(int(sys.argv[-1]))
    n, r, c, padl, padr = (int(cmdlinearg(name)) for name in ['n', 'r', 'c', 'padl', 'padr'])
    assert n > 1
    print("{} {} {}".format(n, r, c))
    trips = []
    for i in range(0, n - padl - padr):
        lo = random.randint(padl, 2*padl)
        hi = random.randint(r - 2*padr, r - padr)
        assert lo <= hi
        mid = (lo + hi) >> 1
        assert mid > 0
        trips.append((mid, 1, min(mid-lo, hi - mid)))
    for i in range(0, padl):
        lo = random.randint(1, padl)
        hi = random.randint(r - 2*padr, r - padr)
        assert lo <= hi
        mid = (lo + hi) >> 1
        assert mid > 0
        trips.append((mid, 1, min(mid-lo, hi-mid)))
    for i in range(0, padr):
        lo = random.randint(padl, 2*padl)
        hi = random.randint(r - padr, r)
        assert lo <= hi
        mid = (lo + hi) >> 1
        assert mid > 0
        trips.append((mid, 1, min(mid-lo, hi-mid)))

    random.shuffle(trips)
    for r, c, d in trips:
        print("{} {} {}".format(r, c, d))

if __name__ == "__main__":
    main()
