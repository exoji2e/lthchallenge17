#!/bin/bash
for r in 2478 6657 4107 5305 5598 
do
    echo $r
    python3 gen_close_worst.py $r > "secret/$r.close_worst.in"
    python3 gen_rand.py $r > "secret/$r.rand.in"
done
python3 gen_rand.py r=10 c=10000 1 > "secret/10-10000.rand.in"
python3 gen_rand.py r=100 c=1000 1 > "secret/100-1000.rand.in"
python3 gen_rand.py r=1000 c=100 1 > "secret/1000-100.rand.in"
python3 gen_rand.py r=10000 c=10 1 > "secret/10000-10.rand.in"
python3 gen_worst.py > "secret/worst.in"
python3 gen_multicol.py > "secret/multicol.in"
