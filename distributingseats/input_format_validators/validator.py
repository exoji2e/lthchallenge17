#!/usr/bin/python

import re
import sys

INT_REGEX = '(([1-9][0-9]*)|0)'
THREE_INTS_REGEX = '^' + ' '.join([INT_REGEX] * 3) + '$'

line = sys.stdin.readline()
assert re.match(THREE_INTS_REGEX, line)
n, r, c  = map(int, line.split())
assert 1 <= n <= 100000
assert 1 <= r <= 100000
assert 1 <= c <= 100000

for i in range(n):
    line = sys.stdin.readline()
    assert re.match(THREE_INTS_REGEX, line)
    r0, c0, d = map(int, line.split())
    assert 0 < r0 <= r
    assert 0 < c0 <= c
    assert 0 <= d <= r

assert len(sys.stdin.readline()) == 0

sys.exit(42)
