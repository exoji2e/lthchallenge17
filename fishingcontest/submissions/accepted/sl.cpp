#include <bits/stdc++.h>
using namespace std;

#define rep(i, from, to) for (int i = from; i < (to); ++i)
#define trav(a, x) for (auto& a : x)
#define all(x) x.begin(), x.end()
#define sz(x) (int)(x).size()
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

template<class F>
void f(int i, int N, F g) {
	for (int j = i; j;) {
		j &= j-1;
		g(j);
	}
	g(i);
	if (i) for (;;) {
		i += i & -i;
		if (i >= N) break;
		g(i);
	}
}

constexpr int code(int i, int j) {
	// assert(abs(i) + abs(j) <= 2);
	return 1 << (i*4 + j + 8);
}

int main() {
	cin.sync_with_stdio(false);
	int H, W, K, T;
	cin >> H >> W >> K >> T;
	vector<vi> times(H, vi(W));

	vector<vector<tuple<int, int>>> fishEvents(T+1);
	auto addFishEvent = [&](int when, int i, int j) {
		if (when >= 0 && when <= T)
			fishEvents[when].emplace_back(i, j);
	};

	vector<vector<tuple<int, int, int>>> reachEvents(T+1);
	auto addReachEvent = [&](int when, int i, int j, int d) {
		if (when >= 0 && when <= T)
			reachEvents[when].emplace_back(i, j, d);
	};

	vector<map<pii, vector<pii>>> localEvents(T+1);

	auto available = [&](int i, int j, int t) {
		if (i < 0 || j < 0 || i >= H || j >= W) return false;
		return times[i][j] <= t && t < times[i][j] + K;
	};

	int si, sj;
	cin >> si >> sj;
	f(si, H, [&](int i2) {
		f(sj, W, [&](int j2) {
			int when = 1 + abs(si - i2) + abs(sj - j2);
			addReachEvent(when, i2, j2, 0);
		});
	});

	rep(i,0,H) {
		rep(j,0,W) {
			int t;
			cin >> t;
			times[i][j] = t;
			rep(k,0,K)
				addFishEvent(t + k, i, j);
		}
	}

	const int inf = 2000000;
	vector<vi> scores(H, vi(W, -inf));
	vector<vector<vector<pair<int, int>>>> hscores(H, vector<vector<pair<int, int>>>(W));
	rep(i,0,H) rep(j,0,W) hscores[i][j].emplace_back(0, scores[i][j]);

	int res = 0;
	rep(t,0,T+1) {
		trav(ev, reachEvents[t]) {
			int i, j, s;
			tie(i, j, s) = ev;
			if (s > scores[i][j]) {
				// Can jump here with this score and no local data.
				scores[i][j] = s;
				auto& v = hscores[i][j];
				if (v.back().first == t)
					v.back().second = s;
				else
					v.emplace_back(t, s);
			}
		}

		trav(ev, fishEvents[t]) {
			int i, j;
			tie(i, j) = ev;

			// a fish appears! maybe we can catch it starting from no
			// local data?
			int s = -inf;
			f(i, H, [&](int i2) {
				f(j, W, [&](int j2) {
					int when = t - abs(i - i2) - abs(j - j2);
					if (when < 0) return;
					// PERF: we could amortize this over the same fish appearing
					// at t, t+1, ..., t+K-1 to win a factor K overall.
					s = max(s, (--upper_bound(all(hscores[i2][j2]), pii(when, inf)))->second);
				});
			});

			if (s == -inf) continue; // too far away :(

			s += 1; // one (1) fish

			// process it locally
			localEvents[t][pii(i,j)].emplace_back(s, code(0,0));
		}

		trav(pij, localEvents[t]) {
			int i, j;
			tie(i,j) = pij.first;

			int availbits = 0;
			rep(di,-2,3) rep(dj,-2,3) {
				int dt = abs(di) + abs(dj);
				if (dt < 3 && available(i+di, j+dj, t+dt))
					availbits |= code(di,dj);
			}
			int midbit = availbits & code(0,0);

			map<int, int> best;
			trav(ev, pij.second) {
				int s, bits;
				tie(s, bits) = ev;
				bits &= availbits;
				if ((bits | midbit) != bits) {
					bits |= midbit;
					s++;
				}
				int& b = best[bits];
				b = max(b, s);
			}

			trav(pa, best) {
				int s, bits;
				tie(bits, s) = pa;

				res = max(res, s);

				if (bits == 0) {
					f(i, H, [&](int i2) {
						f(j, W, [&](int j2) {
							int when = t + abs(i - i2) + abs(j - j2);
							if (when != t)
								addReachEvent(when, i2, j2, s);
						});
					});

					// (Copy-pasted from above!)
					if (s > scores[i][j]) {
						scores[i][j] = s;
						auto& v = hscores[i][j];
						if (v.back().first == t)
							v.back().second = s;
						else
							v.emplace_back(t, s);
					}
				} else if (t+1 <= T) {
					// move
					rep(d,0,4) {
						int ni = i, nj = j;
						constexpr unsigned
							MID = code(0,0) | code(0,1) | code(0,-1) | code(1,0) | code(-1,0),
							BELOW = MID | code(1,-1) | code(1,1) | code(2,0),
							ABOVE = MID | code(-1,-1) | code(-1,1) | code(-2,0),
							RIGHT = MID | code(-1,1) | code(1,1) | code(0,2),
							LEFT = MID | code(-1,-1) | code(1,-1) | code(0,-2);
						unsigned bits2;
						     if (d == 0) ni++, bits2 = (bits & BELOW) >> 4;
						else if (d == 1) ni--, bits2 = (bits & ABOVE) << 4;
						else if (d == 2) nj++, bits2 = (bits & RIGHT) >> 1;
						else if (d == 3) nj--, bits2 = (bits & LEFT) << 1;
						else assert(0);
						if (ni < 0 || nj < 0 || ni >= H || nj >= W) continue;
						localEvents[t+1][pii(ni,nj)].emplace_back(s, bits2);
					}

					// or don't move
					localEvents[t+1][pii(i,j)].emplace_back(s, bits);
				}
			}
		}

		// Why not reuse memory when we can.
		localEvents[t].clear();
		if (t+2 <= T) {
			assert(localEvents[t+2].empty());
			localEvents[t+2].swap(localEvents[t]);
		}
	}

	cout << res << endl;
	exit(0);
}
