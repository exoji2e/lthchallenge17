#include <bits/stdc++.h>
using namespace std;

#define rep(i, a, b) for(int i = a; i < (b); ++i)
#define trav(a, x) for(auto& a : x)
#define all(x) x.begin(), x.end()
#define sz(x) (int)(x).size()
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

//   X
//  XXX
// XXOXX
//  XXX
//   X
vector<vector<pii>> contexts = {
    {},

    {pii(-1, 0)},
    {pii(1, 0)},
    {pii(0, -1)},
    {pii(0, 1)},

    {pii(-1, 0), pii(-2, 0)},
    {pii(1, 0), pii(2, 0)},
    {pii(0, -1), pii(0, -2)},
    {pii(0, 1), pii(0, 2)},

    {pii(-1, 0), pii(-1, -1)},
    {pii(1, 0), pii(1, -1)},
    {pii(0, -1), pii(1, -1)},
    {pii(0, 1), pii(1, 1)},

    {pii(-1, 0), pii(-1, 1)},
    {pii(1, 0), pii(1, 1)},
    {pii(0, -1), pii(-1, -1)},
    {pii(0, 1), pii(-1, 1)},

    {pii(-2, 0)},
    {pii(2, 0)},
    {pii(0, -2)},
    {pii(0, 2)},

    {pii(-1, -1)},
    {pii(-1, 1)},
    {pii(1, -1)},
    {pii(1, 1)},

    {pii(-1, 0), pii(-1, 1), pii(0, 1)},
    {pii(-1, 0), pii(-1, -1), pii(0, -1)},
    {pii(1, 0), pii(1, 1), pii(0, 1)},
    {pii(1, 0), pii(1, -1), pii(0, -1)},

    {pii(0, -1),  pii(0, 1)},
    {pii(-1, 0), pii(1, 0)},
    {pii(-1, 0), pii(0, 1)},
    {pii(-1, 0), pii(0, -1)},
    {pii(1, 0), pii(0, 1)},
    {pii(1, 0), pii(0, -1)},

};

vector<pii> moves = {
    pii(0, 0),

    pii(0, -1),
    pii(0, 1),
    pii(1, 0),
    pii(-1, 0),

    pii(0, -2),
    pii(0, 2),
    pii(2, 0),
    pii(-2, 0),

    pii(1, -1),
    pii(1, 1),
    pii(-1, -1),
    pii(-1, 1)
};

int main() {
    cin.sync_with_stdio(0); cin.tie(0);
    cin.exceptions(cin.failbit);

    trav(it, contexts) sort(all(it));

    int R, C, K, L;
    cin >> R >> C >> K >> L;
    int x0, y0;
    cin >> x0 >> y0;
    vector<vi> t(R, vi(C, -1000));
    rep(i,0,R) rep(j,0,C) cin >> t[i][j];

    vector<vector<pii>> fishtimes(L + K + 1);
    rep(i,0,R) rep(j,0,C) {
        rep(ct, t[i][j], t[i][j] + K) {
            fishtimes[ct].emplace_back(i, j);
        }
    }

    vector<vector<vector<vi>>> state(R,
        vector<vector<vi>>(C,
            vector<vi>(K,
                vi(
                    sz(contexts), -1000
                )
            )
        )
    );

    typedef tuple<int, int, int, int> ST;
    vector<vector<vector<vector<ST>>>> nx(R,
        vector<vector<vector<ST>>>(C,
            vector<vector<ST>>(K,
                vector<ST>(
                    sz(contexts), ST(-1, -1, -1, -1)
                )
            )
        )
    );



    // We process our arrival somewhere at the start of second [curtime, curtime + 1)
    for (int curtime = L; curtime >= 1; --curtime) {
        trav(it, fishtimes[curtime]) {
            // We just arrived to catch fish at it, at the start of [curtime, curtime + 1)

            int MRES = 1;
            ST bst(-1, -1, -1, -1);
            // Try to go somewhere somewhere that is not a move
            rep(i,0,R) rep(j,0,C) {
                int dx = i - it.first,
                    dy = j - it.second;
                if (count(all(moves), pii(dx, dy))) continue;
                int nt = curtime + abs(dx) + abs(dy);
                nt = max(nt, t[i][j]);
                // We must be able to catch the new fish!
                if (nt < t[i][j] + K && nt <= L) {
                    int val = state[i][j][nt - t[i][j]][0] + 1;
                    if (val > MRES) {
                        MRES = val;
                        bst = ST(i, j, nt - t[i][j], 0);
                    }
                }
            }
            // What context do we have?
            rep(ctxi, 0, sz(contexts)) {

                ST bs = bst;
                int RES = MRES;

                // Try to move 1-2 steps
                vector<pii> ctx = contexts[ctxi];
                trav(cit, ctx) {
                    int nx = it.first + cit.first,
                        ny = it.second + cit.second;
                    if (nx < 0 || nx >= R || ny < 0 || ny >= C) goto skip;
                }
                rep(d, 0, sz(moves)) {
                    // Don't visit a fish we have already counted.
                    if (count(all(ctx), moves[d])) continue;
                    int nx = it.first + moves[d].first,
                        ny = it.second + moves[d].second;
                    if (nx < 0 || nx >= R || ny < 0 || ny >= C) continue;

                    int dist = abs(moves[d].first) + abs(moves[d].second);
                    int thetime = curtime + dist;
                    thetime = max(thetime, t[nx][ny]);
                    // Can we get there in time to get a fish?
                    if (thetime >= t[nx][ny] + K || thetime > L) continue;

                    // Compute new context
                    vector<pii> nctx;
                    if (d) nctx.emplace_back(-moves[d].first, -moves[d].second);

                    trav(cit, ctx) {
                        int dx = cit.first - moves[d].first,
                            dy = cit.second - moves[d].second;
                        if (!count(all(moves), pii(dx, dy))) continue;
                        if (thetime + abs(dx) + abs(dy) < t[nx + dx][ny + dy] + K) nctx.emplace_back(dx, dy);
                    }
                    set<pii> uni(all(nctx));
                    nctx.assign(all(uni));

                    int widx = (int)(find(all(contexts), nctx) - contexts.begin());
                    if (widx == sz(contexts)) {
                        int over1 = 0;
                        trav(it, nctx) {
                            if (abs(it.first) + abs(it.second) > 1) ++over1;
                        }
                        assert(over1 >= 2);
                        continue;
                    }
                    int val = max(0, state[nx][ny][thetime - t[nx][ny]][widx]) + !!d;
                    if (val > RES) {
                        RES = val;
                        bs = ST(nx, ny, thetime - t[nx][ny], widx);
                    }
                }
skip:;
                state[it.first][it.second][curtime - t[it.first][it.second]][ctxi] = RES;
                nx[it.first][it.second][curtime - t[it.first][it.second]][ctxi] = bs;
            }
        }
    }

    int ans = 0;
    ST st(-1, -1, -1, -1);
    rep(i,0,R) rep(j,0,C) rep(k,0,K) {
        if (1 + abs(i - x0) + abs(j - y0) <= t[i][j] + k) {
            int val = state[i][j][k][0];
            if (val > ans) {
                ans = val;
                st = ST(i, j, k, 0);
            }
        }
    }
    set<pii> caught;
    int thetime = 1;
    while (st != ST(-1, -1, -1, -1)) {
        int x, y, z, w;
        tie(x, y, z, w) = st;
        // cerr << x << " " << y << " " << endl;
        st = nx[x][y][z][w];

        int d = abs(x0 - x) + abs(y0 - y);
        assert(thetime + d < t[x][y] + K);
        caught.emplace(x, y);
        thetime = max(thetime + d, t[x][y]);
        x0 = x;
        y0 = y;
    }
    cout << ans << endl;
    assert(sz(caught) == ans);
    // cerr << sz(caught) << endl;
}
