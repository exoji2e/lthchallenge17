#!/usr/bin/env python3
# Uniformly distributed values.

from generator import *
import random

n = int(cmdlinearg('n'))
m = int(cmdlinearg('m'))
k = int(cmdlinearg('k'))
t = int(cmdlinearg('t'))
l = int(cmdlinearg('l', t))
base = int(cmdlinearg('base', 1))
x0 = random.randrange(n)
y0 = random.randrange(m)

print(n,m,k,l)
print(x0,y0)

board = [[-1] * m for _ in range(n)]
for i in range(n):
    for j in range(m):
        board[i][j] = random.randrange(base, t+1)

for i in range(n):
    print(*board[i])
