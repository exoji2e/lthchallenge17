#!/usr/bin/env python3
import random
import sys

def cmdlinearg(name, default=None):
    for arg in sys.argv:
        if arg.startswith(name + "="):
            return arg.split("=")[1]
    if default is not None:
        return default
    assert False, "Missing argument: " + name

random.seed(int(sys.argv[-1]))
