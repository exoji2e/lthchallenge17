#!/bin/bash

MAXN=100

rm -rf secret/
mkdir secret/

for k in 1 2 3 4 5; do
	python3 gen_rand.py n=$MAXN m=$MAXN k=$k t=98000 1$k > secret/1-$k.in
	python3 gen_rand.py n=$MAXN m=$MAXN k=$k t=100 2$k > secret/2-$k.in
done
echo done 1 2

python3 gen_rand.py n=$MAXN m=$MAXN k=5 t=30 31 > secret/3-1.in
python3 gen_rand.py n=$MAXN m=$MAXN k=5 t=10 32 > secret/3-2.in
python3 gen_rand.py n=$MAXN m=$MAXN k=3 t=10 33 > secret/3-3.in
python3 gen_rand.py n=1 m=$MAXN k=1 t=100000 34 > secret/3-4.in
python3 gen_rand.py n=$MAXN m=1 k=5 t=100000 35 > secret/3-5.in
python3 gen_rand.py n=$MAXN m=$MAXN k=5 t=10 l=100000 36 > secret/3-6.in
python3 gen_rand.py n=1 m=1 k=5 t=100000 37 > secret/3-7.in
python3 gen_rand.py n=$MAXN m=$MAXN k=5 base=99990 t=100000 38 > secret/3-8.in
python3 gen_rand.py n=$MAXN m=$MAXN k=1 base=99990 t=100000 39 > secret/3-9.in
echo done 3

for k in 1 2 3 4 5; do
	python3 gen_rand_blocks.py n=$MAXN m=$MAXN k=$k t=98000 r=2 s=5 4$k > secret/4-$k.in
	python3 gen_rand_blocks.py n=$MAXN m=$MAXN k=$k t=98000 base=97500 r=3 s=10 4$k > secret/5-$k.in
	python3 gen_rand_blocks.py n=$MAXN m=$MAXN k=$k t=100 r=2 s=5 6$k > secret/6-$k.in
	python3 gen_rand_blocks.py n=10 m=10 k=$k t=50 r=2 s=5 7$k > secret/7-$k.in
	python3 gen_rand_blocks.py n=$MAXN m=$MAXN k=$k t=98000 r=2 s=5 8$k > secret/8-$k.in
done
echo done 4 5 6 7 8

