#!/usr/bin/env python3
# Uniformly distributed values, where each RxR block has approximately the same value (|x - y| <= S).

from generator import *
import random

n = int(cmdlinearg('n'))
m = int(cmdlinearg('m'))
k = int(cmdlinearg('k'))
t = int(cmdlinearg('t'))
r = int(cmdlinearg('r'))
s = int(cmdlinearg('s'))
l = int(cmdlinearg('l', t))
base = int(cmdlinearg('base', 1))
x0 = random.randrange(n)
y0 = random.randrange(m)

rm = (m + r - 1) // r
rn = (n + r - 1) // r
rboard = [[-1] * rm for _ in range(rn)]
for i in range(rn):
    for j in range(rm):
        rboard[i][j] = random.randrange(base, t+1 - s)

board = [[-1] * m for _ in range(n)]
for i in range(n):
    for j in range(m):
        board[i][j] = rboard[i // r][j // r] + random.randrange(s+1)

print(n,m,k,l)
print(x0,y0)

for i in range(n):
    print(*board[i])
