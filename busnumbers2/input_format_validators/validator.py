#!/usr/bin/python

import re
import sys

ONE_INT_REGEX = '^[1-9][0-9]*$'

line = sys.stdin.readline()
assert re.match(ONE_INT_REGEX, line)
m = int(line)
assert 1 <= m <= 400000
assert len(sys.stdin.readline()) == 0
sys.exit(42)
