#include <bits/stdc++.h>
using namespace std;

#define rep(i, a, b) for(int i = a; i < (b); ++i)
#define trav(a, x) for(auto& a : x)
#define all(x) x.begin(), x.end()
#define sz(x) (int)(x).size()
typedef long long ll;
typedef pair<int, int> pii;
typedef vector<int> vi;

int main() {
	cin.sync_with_stdio(0); cin.tie(0);
	cin.exceptions(cin.failbit);

    vector<bool> cube(400000 + 1);
    for (int i = 1; i * i * i <= 400000; ++i) {
        cube[i * i * i] = true;
    }

    int N;
    cin >> N;
    int ans = -1;
    rep(i,1,N+1) {
        int ways = 0;
        for (int j = 1; j * j * j <= i - j * j * j; ++j) {
            int w = i - j * j * j;
            if (cube[w]) ways++;
        }
        if (ways >= 2) ans = i;
    }
    if (ans == -1) cout << "none" << endl;
    else cout << ans << endl;
}
