m = int(input())
once = set()
twice = set([-1])
for i in range(1, 100):
    for j in range(i, 100):
        cb = i*i*i + j*j*j
        if cb in once:
            twice.add(cb)
        elif cb <= m:
            once.add(cb)
if len(twice) == 1: print("none")
else: print(max(twice))
