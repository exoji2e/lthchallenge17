import math
import random

cnt = 1
def wt(peoples):
    global cnt
    tc = "{:02d}.in".format(cnt)
    cnt += 1
    f = open(tc, "w")
    inp = '\n'.join([str(len(peoples))] + [" ".join(str(y) for y in x) for x in peoples] + [""])
    f.write(inp)
    f.close()


def rperson():
    s = random.randint(0, 9)
    p = random.randint(0, 10**9)
    f = random.randint(0, 300)
    o = random.randint(0, 1)
    return [s, p, f, o]

def rperson_small_universe():
    s = random.randint(0, 9)
    p = random.randint(0, 5)
    f = random.randint(0, 5)
    o = random.randint(0, 1)
    return [s, p, f, o]

def rcase():
    ps = random.randint(1, 1000)
    inp = [rperson() for _ in range(ps)]
    wt(inp)

def rscase():
    ps = random.randint(1, 1000)
    inp = [rperson_small_universe() for _ in range(ps)]
    wt(inp)

def rall():
    x = rperson()
    ps = random.randint(1, 1000)
    inp = [x for _ in range(ps)]
    wt(inp)

def mxcase():
    wt([[9, 10**9, 300, 1] for _ in range(1000)])


wt([rperson()])
mxcase()
for i in range(10): rcase()
for i in range(20): rscase()
for i in range(5): rall()
