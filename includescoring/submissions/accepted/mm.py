#!/usr/bin/env python3
n = int(input())
cont = []
for i in range(n):
    s, p, f, o = map(int, input().split())
    cont.append((-s, p, f, o, i))
cont.sort()
scores = ([100, 75, 60, 50, 45, 40, 36, 32, 29, 26, 24, 22, 20, 18, 16] + 
    [i for i in range(15, 0, -1)] + 
    [0 for _ in range(n)])
sc = [0]*n
i = 0
while i < n:
    j = 1
    while i + j < n and cont[i+j][:3] == cont[i][:3]:
        j+=1
    s = sum(scores[i:i+j])
    scc = (s + j - 1)//j
    for x in range(i, i+j):
        sc[cont[x][4]] = cont[x][3] + scc
        #sc[cont[x][4]] = (i, cont[x][3] + scc)
    i += j
    
print('\n'.join(map(str, sc)))
